package org.abbtech.exercise_22;

import org.abbtech.exercise_22.annotations.Secured;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercise22Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercise22Application.class, args);
		TestClass testClass = new TestClass();
		testClass.securedMethod(Secured.Role.ADMIN); // Should print "Authorized" followed by "Inside securedMethod"
		testClass.securedMethod(Secured.Role.USER);  // Should print "Not Authorized"
		testClass.unsecuredMethod();
	}

}
