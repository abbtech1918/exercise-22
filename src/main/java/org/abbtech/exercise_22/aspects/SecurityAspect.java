package org.abbtech.exercise_22.aspects;

import org.abbtech.exercise_22.annotations.Secured;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Before;

public class SecurityAspect {
    @Before("@annotation(secured) && args(role,..)")
    public void checkSecurity(JoinPoint joinPoint, Secured secured, Secured.Role role) {
        if (secured.value() == role) {
            System.out.println("Authorized");
        } else {
            System.out.println("Not Authorized");
        }
    }
}
