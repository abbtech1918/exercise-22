package org.abbtech.exercise_22.aspects;

import org.abbtech.exercise_22.annotations.Loggable;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Before("@annotation(loggable)")
    public void logMethod(JoinPoint joinPoint, Loggable loggable) {
        Loggable.Priority priority = loggable.value();
        System.out.println("Loggable priority: " + priority);
    }
}
