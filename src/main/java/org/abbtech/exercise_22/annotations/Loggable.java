package org.abbtech.exercise_22.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Loggable {
    Priority value() default Priority.MEDIUM;

    enum Priority {
        LOW, MEDIUM, HIGH
    }
}
