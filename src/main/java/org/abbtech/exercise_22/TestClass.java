package org.abbtech.exercise_22;

import org.abbtech.exercise_22.annotations.Loggable;
import org.abbtech.exercise_22.annotations.Secured;

public class TestClass {
    @Secured(Secured.Role.ADMIN)
    public void securedMethod(Secured.Role role) {
        System.out.println("Inside securedMethod");
    }

    @Loggable(Loggable.Priority.HIGH)
    public void unsecuredMethod() {
        System.out.println("Inside loggable");
    }
}
